package com.zeroweb.activity

import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.Signature
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.google.android.gms.common.api.Api
import com.zeroweb.api.ApiService
import com.zeroweb.checkin.checkin_register.R
import com.zeroweb.checkin.checkin_register.R.*
import com.zeroweb.fragment.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.nav_header_main.*
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException



class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, DrawerLayout.DrawerListener {
    val apiService: ApiService = ApiService.create()
    //get key hash for kakao map
    private fun getAppKeyHash() {
        try {
            val info:PackageInfo = packageManager.getPackageInfo("com.example.i_06.checkin_manager", PackageManager.GET_SIGNATURES)
                for (signature: Signature in info.signatures) {
                    val md: MessageDigest = MessageDigest.getInstance("SHA");
                    md.update(signature.toByteArray());
                    Log.d("keyhash", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getAppKeyHash()

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, string.navigation_drawer_open, string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        drawer_layout.addDrawerListener(this)

        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        val mFragment: RegisterPlace = RegisterPlace.newInstance()

        val userId: Int = intent.getIntExtra("userId", 0)
        val deviceUuid: String = intent.getStringExtra("deviceUuid")
        val username: String = intent.getStringExtra("username")
        val bundle = Bundle()
        bundle.run {
            putInt("userId", userId)
            putString("username", username)
            putString("deviceUuid", deviceUuid)
        }
        mFragment.arguments = bundle
        openFragment(mFragment)
    }

    //drawer
    override fun onDrawerStateChanged(p0: Int) {}

    override fun onDrawerSlide(p0: View, p1: Float) {}

    override fun onDrawerClosed(p0: View) {}

    override fun onDrawerOpened(p0: View) {
        val name: String = intent.getStringExtra("fullname")
        val emailStr: String = intent.getStringExtra("email")
        fullname.text = name
        email.text = emailStr
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            id.nav_register_place -> {
                supportActionBar?.title = getString(R.string.register_place)
                val mFragment: RegisterPlace = RegisterPlace.newInstance()
                openFragment(mFragment)
                val userId: Int = intent.getIntExtra("userId", 0)
                val deviceUuid: String = intent.getStringExtra("deviceUuid")
                val username: String = intent.getStringExtra("username")
                val bundle = Bundle()
                bundle.run {
                    putInt("userId", userId)
                    putString("deviceUuid", deviceUuid)
                    putString("username", username)
                }
                mFragment.arguments = bundle

                drawer_layout.closeDrawer(GravityCompat.START)
                return true
            }
//            id.nav_search_current_location -> {
//                supportActionBar?.title = getString(R.string.search_current_location)
//                val mFragment: SearchCurrentLocation = SearchCurrentLocation.newInstance()
//                openFragment(mFragment)
//
//                val deviceUuid: String = intent.getStringExtra("deviceUuid")
//                val bundle = Bundle()
//                bundle.run {
//                    putString("deviceUuid", deviceUuid)
//                }
//                mFragment.arguments = bundle
//                drawer_layout.closeDrawer(GravityCompat.START)
//                return true
//            }
//            id.nav_check_place_location -> {
//                supportActionBar?.title = getString(R.string.check_place_location)
//                val mFragment = CheckPlaceLocation.newInstance()
//                openFragment(mFragment)
//
//                drawer_layout.closeDrawer(GravityCompat.START)
//                return true
//            }
            id.nav_user_history -> {
                supportActionBar?.title = getString(R.string.user_history)
                val mFragment = UserHistory.newInstance()
                openFragment(mFragment)

                val usernameStr = intent.getStringExtra("username")
                val bundle = Bundle()
                bundle.putString("username", usernameStr)
                mFragment.arguments = bundle

                drawer_layout.closeDrawer(GravityCompat.START)
                return true
            }

            id.nav_change_password -> {
                supportActionBar?.title = getString(R.string.change_password)
                val mFragment = ChangePassword.newInstance()
                openFragment(mFragment)

                val usernameStr = intent.getStringExtra("username")
                val bundle = Bundle()
                bundle.putString("username", usernameStr)
                mFragment.arguments = bundle

                drawer_layout.closeDrawer(GravityCompat.START)
                return true
            }
            id.nav_logout -> {
                Toast.makeText(applicationContext, getString(R.string.logout), Toast.LENGTH_LONG).show()
                apiService.signOut()

                // back to login
                val intent = Intent(this, LoginActivity::class.java)
                intent.putExtra("state", "launch")
                startActivity(intent)
                finish()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(com.zeroweb.checkin.checkin_register.R.id.content_frame, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}


