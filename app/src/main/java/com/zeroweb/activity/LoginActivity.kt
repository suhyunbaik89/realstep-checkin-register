package com.zeroweb.activity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.*
import android.content.pm.PackageManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import com.zeroweb.api.ApiService
import com.zeroweb.api.DeviceSerializer
import com.zeroweb.api.UserSerializer
import com.zeroweb.checkin.checkin_register.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*


/**
 * A login screen that offers login via username/password.
 */

class LoginActivity : AppCompatActivity() {
    val apiService = ApiService.create()
    private var mAuthTask: UserLoginTask? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        supportActionBar!!.hide()
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login)
        // Set up the login form.
        password.setOnEditorActionListener(TextView.OnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin()
                return@OnEditorActionListener true
            }
            false
        })

        // autologin
        val loginInfo = getSharedPreferences("setting", Context.MODE_PRIVATE)
        if (loginInfo.contains("auto_login_enabled") && loginInfo.getBoolean("auto_login_enabled", true)) {
            val usernameStr = loginInfo.getString("username", "")
            val passwordStr = loginInfo.getString("password", "")
            email.setText(usernameStr)
            password.setText(passwordStr)
            autologin.isChecked = true
        }

        autologin.setOnCheckedChangeListener {
            buttonView, isChecked ->
            if (isChecked) {
                Toast.makeText(applicationContext, getString(R.string.set_auto_login), Toast.LENGTH_LONG).show()
                val emailStr = email.text.toString()
                val passwordStr = password.text.toString()
                saveAutoLogin(emailStr, passwordStr)

            } else {
                Toast.makeText(applicationContext, getString(R.string.disable_auto_login), Toast.LENGTH_LONG).show()
                clearAutoLogin()
            }
        }

        email_sign_in_button.setOnClickListener { attemptLogin() }
        sign_up.setOnClickListener { attemptSignUp() }

        //get app version
//        val manager = this.packageManager
//        val info = manager.getPackageInfo(this.packageName, PackageManager.GET_ACTIVITIES)
//        app_version.text = info.versionName.toString()
    }

    private fun clearAutoLogin() {
        val loginInfo = getSharedPreferences("setting", Context.MODE_PRIVATE)
        val editor = loginInfo.edit()
        editor.clear()
        editor.apply()
    }

    private fun saveAutoLogin(username: String, password: String) {
        val loginInfo = getSharedPreferences("setting", 0)
        val editor = loginInfo.edit()
        editor.putBoolean("auto_login_enabled", true)
        editor.putString("username", username)
        editor.putString("password", password)
        editor.apply()
    }

    private fun saveDeviceInfo(deviceUuid: String) {
        val deviceInfo = getSharedPreferences("device", 0)
        val editor = deviceInfo.edit()
        editor.putBoolean("is_registered", true)
        editor.putString("deviceUuid", deviceUuid)
        editor.apply()
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
    }

        /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */

    @SuppressLint("CheckResult")
    private fun attemptLogin() {
        if (mAuthTask != null) {
            return
        }

        // Reset errors.
        email.error = null
        password.error = null

        // Store values at the time of the login attempt.
        val emailStr = email.text.toString()
        val passwordStr = password.text.toString()

        var cancel = false
        var focusView: View? = null

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(passwordStr)) {
            password.error = getString(R.string.error_invalid_password)
            focusView = password
            cancel = true
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(emailStr)) {
            email.error = getString(R.string.error_field_required)
            focusView = email
            cancel = true

        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView?.requestFocus()
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true)
            mAuthTask = UserLoginTask(emailStr, passwordStr)
            mAuthTask!!.execute(null as Void?)

            val user = UserSerializer()
            user.username = emailStr
            user.password = passwordStr

            val apiService = ApiService.create()
            apiService.signIn(user)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({user ->
                    val devicePref = getSharedPreferences("device", Context.MODE_PRIVATE)
                    val isRegistered = devicePref.getBoolean("is_registered", false)
                    if (isRegistered) {
                        val deviceUuid: String = devicePref.getString("deviceUuid", "")
                        val intent = Intent(this, MainActivity::class.java)
                        intent.run {
                            putExtra("fullname", user.data!!.fullname)
                            putExtra("username", user.data!!.username)
                            putExtra("email", user.data!!.email)
                            putExtra("userId", user.data!!.userId)
                            intent.putExtra("deviceUuid", deviceUuid)
                        }
                        startActivity(intent)
                        finish()
                    } else {
                        registerDevice {
                        val intent = Intent(this, MainActivity::class.java)
                        intent.run {
                            putExtra("fullname", user.data!!.fullname)
                            putExtra("username", user.data!!.username)
                            putExtra("email", user.data!!.email)
                            putExtra("userId", user.data!!.userId)
                            intent.putExtra("deviceUuid", it)
                        }
                        saveDeviceInfo(it)
                        startActivity(intent)
                        finish()
                    }
                    }

                }, { error ->
                    error.printStackTrace()
                    Toast.makeText(applicationContext, getString(R.string.login_fail), Toast.LENGTH_LONG).show()
                })
        }
    }

    @SuppressLint("CheckResult")
    private fun registerDevice(callBack:(device_uuid: String) -> Unit) {
        val newDevice = DeviceSerializer()
        newDevice.osType = "android"
        newDevice.osVersion = Build.VERSION.RELEASE
        newDevice.model = Build.MODEL
        newDevice.identifier = android.os.Build.SERIAL

        apiService.registerDevice(newDevice)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({response ->
                val device_uuid: String = response.data!!.deviceUuid
                callBack(device_uuid)
                }, { error ->
                    error.printStackTrace()
                })

    }

    private fun attemptSignUp () {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://checkin.zero0web.cloud/app/join"))
        startActivity(intent)
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @SuppressLint("ObsoleteSdkInt")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime).toLong()

            login_form.visibility = if (show) View.GONE else View.VISIBLE
            login_form.animate()
                .setDuration(shortAnimTime)
                .alpha((if (show) 0 else 1).toFloat())
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        login_form.visibility = if (show) View.GONE else View.VISIBLE
                    }
                })

            login_progress.visibility = if (show) View.VISIBLE else View.GONE
            login_progress.animate()
                .setDuration(shortAnimTime)
                .alpha((if (show) 1 else 0).toFloat())
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        login_progress.visibility = if (show) View.VISIBLE else View.GONE
                    }
                })
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            login_progress.visibility = if (show) View.VISIBLE else View.GONE
            login_form.visibility = if (show) View.GONE else View.VISIBLE
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    inner class UserLoginTask internal constructor(private val mEmail: String, private val mPassword: String) :
        AsyncTask<Void, Void, Boolean>() {

        override fun doInBackground(vararg params: Void): Boolean? {
            return true
        }

        override fun onPostExecute(success: Boolean?) {
            mAuthTask = null
            showProgress(false)

            if (success!!) {
                finish()
            } else {
                password.error = getString(R.string.error_incorrect_password)
                password.requestFocus()
            }
        }

        override fun onCancelled() {
            mAuthTask = null
            showProgress(false)
        }
    }

    companion object {}
}

