package com.zeroweb.model

class Spot {
    var distAddr: String = ""
    var roadAddr: String = ""
    var floor: String = ""
    var lat: Double = 0.0
    var lng: Double = 0.0
    var name: String = ""
    var registeredDate: String = ""
}

class NewSpot {
    var distAddr: String = ""
    var roadAddr: String = ""
    var floor: String = ""
    var lat: Double = 0.0
    var lng: Double = 0.0
    var name: String = ""
    var registeredDate: Int = 0
    var updatedDate: Int = 0
    var spotId: Int = 0
    var userFullname: String = ""
    var userId: Int = 0
    var deviceUuid: String = ""
    var categoryId: Int = 0
    var categoryName: String = ""
    var tag: String = ""
    var partnerId: String = ""
//    "partnerName": "",
//    "wlanDatas": null,
//    "bleDatas": null,
    var disabled: Boolean = false
}

class Overview {
    var weekday: String = ""
    var date: String = ""
    var able: Int = 0
    var disabled: Int = 0
    var total_count: Int = 0
}
