package com.zeroweb.model

import java.util.*

class UserInfo {
    var userId : Int = 0
    var username : String = ""
    var fullname : String = ""
    var companyName : String = ""
    var email : String = ""
    var phone : String = ""
    var status : String = ""
    var createdDate : String = ""
    var updatedDate : String = ""
    var lastLoggedInDate : String = ""
}

class CategoryInfo {
    private val categories = ArrayList<Categories>()
}

class Categories {
    var categoryId: Int = 0
    var name: String = ""
}

class DeviceInfo {
    var adid : String = ""
    var osType : String = ""
    var deviceUuid : String = ""
    var model : String = ""
    var osVersion : String = ""
    var registeredDate : String = ""
    var updatedDate : String = ""
}
