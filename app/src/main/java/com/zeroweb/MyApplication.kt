package com.zeroweb

import android.app.Application

class MyApplication: Application() {
    companion object {
        lateinit var instance: MyApplication
    }

    override fun onCreate() {
        instance = this
        super.onCreate()
    }
}