package com.zeroweb.utils

import com.amazonaws.ClientConfiguration
import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.PutObjectRequest
import java.io.File
import java.lang.Exception


class S3Uploader(private val path: File, private val fileName: String) : Thread(){
    companion object {
        private val BUCKET_NAME = "realstep-mobile-captures"
        private val ACCESS_KEY = "AKIAIUX3FNRWIDCLVLLQ"
        private val SECRET_KEY = "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"
    }

    private val awsCredentials: AWSCredentials
    @get:Synchronized
    @set:Synchronized
    var result: Boolean = false

    init {
        this.awsCredentials = BasicAWSCredentials(
            ACCESS_KEY,
            SECRET_KEY
        )
    }

    override fun run() {
        val clientConfiguration = ClientConfiguration()
            clientConfiguration.connectionTimeout = 300 * 1000
            clientConfiguration.socketTimeout = 300 * 1000
            clientConfiguration.maxErrorRetry = 3

        val amazonS3 = AmazonS3Client(awsCredentials, clientConfiguration)
        amazonS3.endpoint = "s3-ap-northeast-2.amazonaws.com"

        try {
            // upload file
            val putObjectRequest = PutObjectRequest(BUCKET_NAME, fileName, path)
            amazonS3.putObject(putObjectRequest)
        } catch (e: Exception) {
            path.delete()
            result = false
        }
        path.delete()
        result = true

    }

}



