package com.zeroweb.utils

import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.Signature
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Address
import android.location.Geocoder
import android.util.Log
import com.zeroweb.MyApplication
import java.io.ByteArrayOutputStream
import java.io.File
import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*


object Utils {
     fun reverseGeocoding(lat: Double, lng: Double, context: Context): String? {
         val address: List<Address> = Geocoder(context).getFromLocation(lat, lng, 1)
         return address[0].getAddressLine(0)
    }

     fun convertStringToUUID(target: String): UUID {
        val sliced: String = target.replace("-", "")
        return UUID(BigInteger(sliced.substring(0, 16), 16).toLong(), BigInteger(sliced.substring(16), 16).toLong())
    }

    fun fileToByteArray(file: File): ByteArray {
        val filePath: String = file.path
        val bitmap: Bitmap = BitmapFactory.decodeFile(filePath)

        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        return stream.toByteArray()
    }

    fun translateWeekday(weekday: String): String {
        val systemLocale: Locale = MyApplication.instance.resources.configuration.locale
        if (systemLocale.language == "ja") {
            if (weekday == "월") return "月曜日"
            if (weekday == "화") return "火曜日"
            if (weekday == "수") return "水曜日"
            if (weekday == "목") return "木曜日"
            if (weekday == "금") return "金曜日"
            if (weekday == "토") return "土曜日"
            if (weekday == "일") return "日曜日"

        }
        return weekday
    }



}

