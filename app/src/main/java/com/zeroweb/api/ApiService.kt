package com.zeroweb.api

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.zeroweb.*
import com.zeroweb.model.Overview
import io.reactivex.Flowable
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*



interface ApiService {

    @Headers(value = ["Content-type: application/json", "APIKEY: 2/0ddGT6kujE/HP/BFpdAQ==", "AppVersion: 1.0.14"])
    @POST("app/api/account/login")
    fun signIn(@Body requestBody: UserSerializer): Observable<User>

    @Headers(value = ["Content-type: application/json", "APIKEY: 2/0ddGT6kujE/HP/BFpdAQ==", "AppVersion: 1.0.14"])
    @GET("app/api/logout")
    fun signOut(): Flowable<JSONObject>

    @Headers(value = ["Content-type: application/json", "APIKEY: 2/0ddGT6kujE/HP/BFpdAQ==", "AppVersion: 1.0.14"])
    @POST("app/api/account/change/password")
    fun changePassword(@Body requestBody: PasswordSerializer): Observable<User>

    @Headers(value = ["Content-type: application/json", "APIKEY: 2/0ddGT6kujE/HP/BFpdAQ==", "AppVersion: 1.0.14"])
    @POST("app/api/category/list")
    fun categoryList(@Body requestBody: CategorySerializer): Observable<Category>

    @Headers(value = ["Content-type: application/json", "APIKEY: 2/0ddGT6kujE/HP/BFpdAQ==", "AppVersion: 109"])
    @POST("app/api/device/register")
    fun registerDevice(@Body requestBody: DeviceSerializer): Observable<Device>

    @Headers(value = ["Content-type: application/json", "APIKEY: 2/0ddGT6kujE/HP/BFpdAQ==", "AppVersion: 109"])
    @POST("app/api/spot/list")
    fun getSpotList(@Body requestBody: BoundsWrapperSerializer): Observable<SpotList>

    @Headers(value = ["Content-type: application/json", "APIKEY: 2/0ddGT6kujE/HP/BFpdAQ==", "AppVersion: 112"])
    @POST("app/api/spot/register")
    fun registerSpot(@Body requestBody: SpotSerializer): Observable<NewSpotResponse>

    @Headers(value = ["APIKEY: 2/0ddGT6kujE/HP/BFpdAQ==", "AppVersion: 1.0.14"])
    @GET("app/api/user/spot/history/overview")
    fun getSpotHistoryOverview(@Query("username") username: String): Observable<List<Overview>>

    @Headers(value = ["APIKEY: 2/0ddGT6kujE/HP/BFpdAQ==", "AppVersion: 109"])
    @GET("app/api/user/spot/history/detail")
    fun getSpotHistoryDetail(@Query("username") username: String, @Query("page") page: Int, @Query("selected_date") selected_date: String): Observable<HistoryDetail>

    @Headers(value = ["APIKEY: 2/0ddGT6kujE/HP/BFpdAQ==", "AppVersion: 109", "wlanScanType: FIVE_MEDIAN"])
    @POST("app/api/visit/checkin")
    fun registerVisit(@Body requestBody: VisitSerializer): Observable<VisitResponse>


    /* compaion object to create the ApiService */
    companion object Factory {
        fun create(): ApiService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://checkin.zeroweb.cloud/")
                .client(createOkHttpClient())
                .build()

            return retrofit.create(ApiService::class.java)
        }

        private fun createOkHttpClient(): OkHttpClient {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            return OkHttpClient.Builder().addInterceptor(interceptor).addNetworkInterceptor(StethoInterceptor()).build()
        }

    }
}

