package com.zeroweb.api
import com.google.gson.annotations.SerializedName
import java.util.*


class UserSerializer {
    @SerializedName("username")
    var username: String ? = null

    @SerializedName("password")
    var password: String ? = null
}

class PasswordSerializer {
    @SerializedName("username")
    var username: String ? = null

    @SerializedName("password")
    var password: String ? = null

    @SerializedName("newPasswd")
    var newPasswd: String ? = null
}


class CategorySerializer {
    @SerializedName("keyword")
    var keyword: String ? = null

    @SerializedName("lang")
    var lang: String ? = null
}


class DeviceSerializer {
    @SerializedName("adid")
    var adid: String ? = null

    @SerializedName("osType")
    var osType: String ? = null

    @SerializedName("osVersion")
    var osVersion: String ? = null

    @SerializedName("model")
    var model: String ? = null

    @SerializedName("identifier")
    var identifier: String ? = null
}

class BoundsWrapperSerializer {
    @SerializedName("bounds")
    var bounds: BoundsSerializer? = null
}

class BoundsSerializer {
    @SerializedName("ha")
    var ha: Double? = null

    @SerializedName("ba")
    var ba: Double ? = null

    @SerializedName("fa")
    var fa: Double ? = null

    @SerializedName("ga")
    var ga: Double ? = null
}

class SpotSerializer {
    @SerializedName("bleDatas")
    var bleDates: List<Object>? = null

    @SerializedName("deviceUuid")
    var deviceUuid: UUID ?= null

    @SerializedName("categoryId")
    var categoryId: Int? = 0

    @SerializedName("distAddr")
    var distAddr: String? = ""

    @SerializedName("floor")
    var floor: String? = ""

    @SerializedName("lat")
    var lat: Double? = null

    @SerializedName("lng")
    var lng: Double? = null

    @SerializedName("name")
    var name: String? = ""

    @SerializedName("partnerId")
    val partnerId: String? = ""

    @SerializedName("partnerName")
    val partnerName: String? = ""

    @SerializedName("roadAddr")
    var roadAddr: String? = ""

    @SerializedName("tag")
    var tag: String? = ""

    @SerializedName("userId")
    var userId: Int? = 0

    @SerializedName("wlanDatas")
    var wlanDatas: List<WlanData> ?= null
}


class VisitSerializer {
    @SerializedName("deviceUuid")
    var deviceUuid: String ?= null

    @SerializedName("wlanDatas")
    var wlanDatas: List<WlanData> ?= null
}


class WlanData {
    @SerializedName("bssid")
    var bssid: String? = null

    @SerializedName("capabilities")
    var capabilities: String? = null

    @SerializedName("dupeCount")
    val dupeCount: Int? = null

    @SerializedName("frequency")
    var frequency: Int? = null

    @SerializedName("level")
    var level: Int? = null

    @SerializedName("ssid")
    var ssid: String? = null
}

