package com.zeroweb.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_register_place.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.net.ConnectivityManager
import android.net.wifi.ScanResult
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.zeroweb.api.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import android.widget.BaseAdapter
import android.widget.TextView
import android.widget.Toast
import com.androidhiddencamera.CameraConfig
import com.androidhiddencamera.CameraError
import com.androidhiddencamera.HiddenCameraFragment
import com.androidhiddencamera.HiddenCameraUtils
import com.androidhiddencamera.config.*
import com.google.android.gms.location.FusedLocationProviderClient

import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.zeroweb.MyApplication
import com.zeroweb.NewSpotResponse
import com.zeroweb.checkin.checkin_register.R
import com.zeroweb.model.Spot
import com.zeroweb.utils.MapApiConst
import com.zeroweb.utils.S3Uploader
import com.zeroweb.utils.Utils.convertStringToUUID
import com.zeroweb.utils.Utils.reverseGeocoding
import kotlinx.android.synthetic.main.activity_login.*
import kr.vhd.wifiscanmanager.ScanFinishListener
import kr.vhd.wifiscanmanager.ScanManager
import kr.vhd.wifiscanmanager.ScanResultSetListener
import kr.vhd.wifiscanmanager.ScanResultUtils
import net.daum.mf.map.api.MapView as DaumMapView
import net.daum.mf.map.api.MapPOIItem
import net.daum.mf.map.api.MapPoint
import net.daum.mf.map.api.MapPointBounds
import java.text.SimpleDateFormat
import java.io.*
import java.util.*


class RegisterPlace : HiddenCameraFragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener, LocationListener,
    GoogleMap.OnMyLocationClickListener, GoogleMap.OnMyLocationButtonClickListener,
    GoogleMap.OnCameraChangeListener, DaumMapView.MapViewEventListener,
    ScanFinishListener, ScanResultSetListener {

    override fun onMyLocationClick(p0: Location) {}

    override fun onMyLocationButtonClick(): Boolean { return false }

    override fun onMapViewDoubleTapped(p0: net.daum.mf.map.api.MapView?, p1: MapPoint?) {}

    override fun onMapViewInitialized(p0: net.daum.mf.map.api.MapView?) {}

    override fun onMapViewDragStarted(p0: net.daum.mf.map.api.MapView?, p1: MapPoint?) {}

    override fun onMapViewMoveFinished(p0: net.daum.mf.map.api.MapView?, p1: MapPoint?) {
        val bounds: MapPointBounds = daumMap.mapPointBounds
        addNearbySpotsDaum(bounds)
    }

    override fun onMapViewCenterPointMoved(p0: net.daum.mf.map.api.MapView?, p1: MapPoint?) {
        val bounds: MapPointBounds = daumMap.mapPointBounds
        addNearbySpotsDaum(bounds)
    }

    override fun onMapViewDragEnded(p0: net.daum.mf.map.api.MapView?, p1: MapPoint?) {}

    override fun onMapViewSingleTapped(p0: net.daum.mf.map.api.MapView?, p1: MapPoint?) {}

    override fun onMapViewZoomLevelChanged(p0: net.daum.mf.map.api.MapView?, p1: Int) {
        val bounds: MapPointBounds = daumMap.mapPointBounds
        addNearbySpotsDaum(bounds)
    }

    override fun onMapViewLongPressed(p0: net.daum.mf.map.api.MapView?, p1: MapPoint?) {}

    val apiService: ApiService = ApiService.create()
    private var isUnderground: Boolean = false
    private var userId: Int ? = 0
    private var username: String ? = ""
    private var deviceUuid: String ? = ""
    private var DIALOG_REQUEST_CODE: Int = 1
    private var category_id: Int = 0
    private var lat: Double ? = null
    private var lng: Double ? = null
    private var googleMap: GoogleMap? = null
    lateinit var daumMap: DaumMapView
    private var cameraImage: File? = null
    var mContext: Context? = activity

    override fun onCameraChange(p0: CameraPosition?) {
        lat = p0!!.target.latitude
        lng = p0.target.longitude
    }

    private fun addNearbySpots(southwestLat: Double, southwestLong: Double, northeastLat: Double, northeastLong: Double) {
        val boundsForRequest = BoundsSerializer()
        boundsForRequest.ba = southwestLong
        boundsForRequest.ha = southwestLat
        boundsForRequest.fa = northeastLong
        boundsForRequest.ga = northeastLat

        val request = BoundsWrapperSerializer()
        request.bounds = boundsForRequest

        apiService.getSpotList(request)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({spotList ->
                for (i:Spot in spotList.data) {
                    googleMap!!.addMarker(MarkerOptions().position(LatLng(i.lat,i.lng)).title(i.name).snippet(i.roadAddr).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)))
                    spot_list.adapter = SpotListAdapter(MyApplication.instance, spotList.data)
                }
            }, { error ->
                error.printStackTrace()
            })
    }

     private fun addNearbySpotsDaum(bounds: MapPointBounds) {
        val boundsForRequest = BoundsSerializer()
        boundsForRequest.ba = bounds.bottomLeft.mapPointGeoCoord.longitude
        boundsForRequest.ha = bounds.bottomLeft.mapPointGeoCoord.latitude
        boundsForRequest.fa = bounds.topRight.mapPointGeoCoord.longitude
        boundsForRequest.ga = bounds.topRight.mapPointGeoCoord.latitude

        val request = BoundsWrapperSerializer()
        request.bounds = boundsForRequest

        apiService.getSpotList(request)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({spotList ->
                for (i:Spot in spotList.data) {
                    val marker = MapPOIItem()
                    marker.itemName = i.name
                    marker.mapPoint = MapPoint.mapPointWithGeoCoord(i.lat, i.lng)
                    daumMap!!.addPOIItem(marker)
                    spot_list.adapter = SpotListAdapter(MyApplication.instance, spotList.data)
                }
            }, { error ->
                error.printStackTrace()
            })
    }


    override fun onImageCapture(imageFile: File) {
        cameraImage = imageFile
    }

    override fun onCameraError(errorCode: Int) {
        val throwable: Throwable = when (errorCode) {
            CameraError.ERROR_CAMERA_OPEN_FAILED -> {
                //Camera open failed. Probably because another application
                //is using the camera
                Throwable("Camera open failed")
            }
            CameraError.ERROR_IMAGE_WRITE_FAILED -> {
                //Image write failed. Please check if you have provided WRITE_EXTERNAL_STORAGE permission
                if (ActivityCompat.checkSelfPermission(MyApplication.instance, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Throwable("File write failed")
                } else {
                    Throwable("File write failed, Permission denied.")
                }
            }
            CameraError.ERROR_CAMERA_PERMISSION_NOT_AVAILABLE -> {
                //camera permission is not available
                //Ask for the camra permission before initializing it.
                Throwable("Camera permission no available")
            }
            CameraError.ERROR_DOES_NOT_HAVE_OVERDRAW_PERMISSION -> {
                //Display information dialog to the user with steps to grant "Draw over other app"
                //permission for the app.
                HiddenCameraUtils.openDrawOverPermissionSetting(MyApplication.instance)
                Throwable("Does not have overdraw permission")
            }

            CameraError.ERROR_DOES_NOT_HAVE_FRONT_CAMERA -> {
                Throwable("Does not have front camera")
            }
            else -> Throwable("Unknown error code return when open camera")
        }
//        Utils.exceptionHandler(throwable)
    }


    private var scanCount = 0
    override fun onScanFinish(results: List<ScanResult>) {
        Toast.makeText(this.context, "${++scanCount} ${getString(R.string.scanning)}", Toast.LENGTH_LONG).show()
    }

    override fun onScanResultSetAvailable(results: List<List<ScanResult>>) {
        scanCount = 0
        val medianResults = ScanResultUtils.resultSetToMedianResult(results)
        return registerNewSpot(medianResults = medianResults)
    }

    override fun onMarkerClick(p0: Marker?): Boolean {return false}

    override fun onLocationChanged(location: Location?) {}

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

    override fun onProviderEnabled(provider: String?) {}

    override fun onProviderDisabled(provider: String?) {}

    override fun onMapReady(gMap: GoogleMap) {
        googleMap = gMap
        gMap.uiSettings.isZoomControlsEnabled = true
        gMap.uiSettings.isMyLocationButtonEnabled = true
        gMap.isMyLocationEnabled = true
        gMap.setOnMyLocationButtonClickListener(this)
        gMap.setOnMyLocationClickListener(this)


        val fusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MyApplication.instance)
        try {
            fusedLocationClient.lastLocation.addOnSuccessListener { location ->
                if (location != null) {
                    gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.latitude, location.longitude), 19.0f))
                    gMap.setOnCameraMoveStartedListener {
                        val bounds = gMap.projection.visibleRegion.latLngBounds
                        addNearbySpots(bounds.southwest.latitude, bounds.southwest.longitude, bounds.northeast.latitude, bounds.northeast.longitude)
                        lat = bounds.center.latitude
                        lng = bounds.center.longitude

                        //if system langauge is japanese
                        val systemLocale: Locale = MyApplication.instance.resources.configuration.locale
                        if (systemLocale.language == "ja") {
                            current_place_address.text = reverseGeocoding(bounds.center.latitude, bounds.center.longitude, MyApplication.instance)
                        }
                    }
                }
            }
        } catch (e:SecurityException) {
            e.printStackTrace()
        }
        // add marker when googlmap loaded for the first time
        gMap.setOnMapLoadedCallback {
            val bounds = gMap.projection.visibleRegion.latLngBounds
            addNearbySpots(bounds.southwest.latitude, bounds.southwest.longitude, bounds.northeast.latitude, bounds.northeast.longitude)
        }
    }
    private var listener: ChangePassword.OnFragmentInteractionListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View = inflater.inflate(com.zeroweb.checkin.checkin_register.R.layout.fragment_register_place, container, false)
        (childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?)?.getMapAsync(this)

        daumMap = DaumMapView(mContext)
        daumMap.onSurfaceDestroyed()

        val mapViewContainer: ViewGroup = (view.rootView.findViewById(R.id.map_view) as? ViewGroup)!!
        mapViewContainer.addView(daumMap)
        daumMap.setDaumMapApiKey(MapApiConst.DAUM_MAPS_ANDROID_APP_API_KEY)
        daumMap.mapType = net.daum.mf.map.api.MapView.MapType.Standard

        val fusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MyApplication.instance)
        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location ->
            daumMap.setMapCenterPointAndZoomLevel(MapPoint.mapPointWithGeoCoord(location.latitude, location.longitude),1, true)
            daumMap.zoomIn(true)
            daumMap.setMapViewEventListener(this)
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        chooseMapByLanguage()
        hideComponentByLanguage()
        userId = arguments?.getInt("userId")
        username = arguments?.getString("username")
        deviceUuid = arguments?.getString("deviceUuid")

        // place is underground
        underground.setOnCheckedChangeListener { _, _ -> isUnderground = true }
        // get selected category
        search_category.setOnClickListener {
            val dialog = SearchCategory()
            dialog.setTargetFragment(this, DIALOG_REQUEST_CODE)
            dialog.show(fragmentManager, "dialog")
        }

        register_place_btn.setOnClickListener {
            if (lat == null) {
                Toast.makeText(this.context, getString(R.string.failed_to_get_gps_info), Toast.LENGTH_LONG).show()
            } else {
                startScan()
                Toast.makeText(this.context, getString(R.string.register_place_complete), Toast.LENGTH_LONG).show()
            }
        }

    }

    override fun onResume() {
        super.onResume()
        // Capture camera
        val cameraConfig: CameraConfig = CameraConfig()
            .getBuilder(MyApplication.instance.applicationContext)
            .setCameraFacing(CameraFacing.REAR_FACING_CAMERA)
            .setCameraResolution(CameraResolution.LOW_RESOLUTION)
            .setImageFormat(CameraImageFormat.FORMAT_JPEG)
            .setImageRotation(CameraRotation.ROTATION_90)
            .setCameraFocus(CameraFocus.AUTO)
            .build()
        startCamera(cameraConfig)

    }

    override fun onPause() {
        super.onPause()
        stopCamera()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val bundle: Bundle = data!!.extras
            val category_name: String = bundle.getString("category_name");
            category_id = bundle.getInt("category_id");
            selected_category.text = category_name
        }
    }

    private val scanManager by lazy { ScanManager(MyApplication.instance) }
    private fun startScan() {
        scanManager.setScanFinishListener(this)
        scanManager.setScanResultSetListener(this)
        scanManager.startScan(7)

        takePicture()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
        daumMap.onSurfaceDestroyed()
    }

    private fun chooseMapByLanguage() {
        val systemLocale: Locale = MyApplication.instance.resources.configuration.locale
        /*
        * choose map by language setting
        * ja : Google map
        * kr : Daum map
        * */
        if (systemLocale.language == "ja") {
            daum_component.visibility = View.GONE
        } else {
            google_component.visibility = View.GONE
        }
    }

    private fun hideComponentByLanguage() {
        val systemLocale: Locale = MyApplication.instance.resources.configuration.locale
        if (systemLocale.language == "ja") {
            spot_list.visibility = View.GONE
        }
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    fun isNetworkConnected(context: Context): Boolean {
        var isConnected = false

        val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val mobile = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
        val wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)

        isConnected = mobile.isConnected || wifi.isConnected
        return isConnected
    }

    fun registerNewSpot(medianResults: List<ScanResult>) {
         //wifi
        val wlans: MutableList<WlanData> = mutableListOf<WlanData>()
        medianResults.forEach {
            if (it.SSID != null && it.BSSID != null) {
                var wlandata = WlanData()
                wlandata.ssid = it.SSID
                wlandata.bssid = it.BSSID
                wlandata.frequency = it.frequency
                wlandata.level = it.level
                wlandata.capabilities = it.capabilities
                wlans.add(wlandata)
            }
        }

        val new_spot: SpotSerializer = SpotSerializer()
        new_spot.wlanDatas = wlans
        new_spot.userId = userId
        new_spot.deviceUuid = convertStringToUUID(this.deviceUuid!!)
        new_spot.name = new_name.text.toString().trim()
        new_spot.tag = ""
        new_spot.floor = if (isUnderground) "B%s".format(new_floor.text.toString()) else new_floor.text.toString()
        new_spot.lat = lat
        new_spot.lng = lng
        new_spot.categoryId = category_id
        new_spot.roadAddr = reverseGeocoding(lat!!, lng!!, this.context!!)

        val s = SimpleDateFormat("yyyy-MM-dd-hh-mm-ss")
        val fileName: String = s.format(Date()) + "_" + username + "_" + new_spot.userId + "_" + new_spot.name + ".jpg"
        val path: File = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        // annotate for temp
//        val file = File(path, fileName)
//        val byteImage: ByteArray = fileToByteArray(this.cameraImage!!)
//        try {
//            val fos = FileOutputStream(file)
//            fos.write(byteImage)
//        } catch (e: FileNotFoundException) {
//            e.printStackTrace()
//        } catch (e: IOException) {
//            e.printStackTrace()
//        }

        val t = S3Uploader(path = cameraImage!!, fileName = fileName)
        t.start()
        try {
            t.join()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        //remove file
        path.delete()

        apiService.registerSpot(new_spot)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({res: NewSpotResponse ->
                Toast.makeText(MyApplication.instance, getString(R.string.registration_completed), Toast.LENGTH_LONG).show()

                //clear text
                new_name.text.clear()
                selected_category.text = getString(R.string.choose_category)
                new_floor.text.clear()

            }, { error: Throwable ->
                Toast.makeText(MyApplication.instance, getString(R.string.registration_failed), Toast.LENGTH_LONG).show()
                error.printStackTrace()
            })
    }

    companion object {
        fun newInstance(): RegisterPlace = RegisterPlace()
    }
}


class SpotListAdapter(val context: Context, val item: ArrayList<Spot>) : BaseAdapter(){
    override fun getView(position: Int, converterView: View?, parent: ViewGroup?): View {
        val view: View = LayoutInflater.from(context).inflate(R.layout.spot_list_item, null)

        val spotName: TextView = view.findViewById(R.id.spot_name)
        val spot: Spot = item[position]

        spotName.text = "%s %sF".format(spot.name, spot.floor)
        return view
    }

    override fun getItem(position: Int): Spot = item[position]

    override fun getItemId(position: Int): Long {return item.indexOf(getItem(position)).toLong()}

    override fun getCount(): Int = item.size
}
