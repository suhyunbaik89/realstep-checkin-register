package com.zeroweb.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.support.v4.app.Fragment
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.TextView
import com.zeroweb.api.ApiService
import com.zeroweb.HistoryDetail
import com.zeroweb.checkin.checkin_register.R
import com.zeroweb.model.Overview
import com.zeroweb.utils.Utils.translateWeekday
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_user_history.*


class UserHistory : Fragment() {
    private var username: String? = ""
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_user_history, container, false)
    }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        username = arguments?.getString("username");
        fun context(): Context ?= activity?.applicationContext
        val apiService = ApiService.create()
        apiService.getSpotHistoryOverview(username!!)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({overview: List<Overview> ->
                history_list.adapter = HistoryAdapter(context()!!, overview)
            }, { error: Throwable ->
                error.printStackTrace()
            })

        history_list.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val selectedItem = parent.getItemAtPosition(position) as Overview
            val dialog = UserHistoryDetail()

            apiService.getSpotHistoryDetail(username=username!!, page = 1, selected_date = selectedItem.date)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({historyDetail: HistoryDetail ->
                    showDetail(historyDetail, dialog, selectedItem.date)
                }, { error: Throwable ->
                    error.printStackTrace()
                })

        }
    }

    private fun showDetail(historyDetail: HistoryDetail, dialog: UserHistoryDetail, selectedDate: String) {
        val bundle = Bundle()
        bundle.run {
            putString("date", historyDetail.date)
            putString("weekday", historyDetail.weekday)
            putString("count", historyDetail.count.toString())
            putSerializable("history", historyDetail.history)
            putString("selectedDate", selectedDate)
            putString("username", username)
        }
        dialog.arguments = bundle
        dialog.show(fragmentManager, "dialog")
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        lateinit var instance: UserHistory
        private set
        fun newInstance(): UserHistory = UserHistory()
    }
}



class HistoryAdapter(val context: Context, val item: List<Overview>) : BaseAdapter(){

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View = LayoutInflater.from(context).inflate(R.layout.user_history_item, null)

        val weekday : TextView = view.findViewById<TextView>(R.id.weekday)
        val date : TextView = view.findViewById<TextView>(R.id.date)
        val body : TextView = view.findViewById<TextView>(R.id.body)

        val day = item[position]
        weekday.text = "(%s)".format(translateWeekday(day.weekday))
        date.text = day.date
        body.text = createBody(day.total_count, day.disabled)
       return view

    }
    override fun getItem(position: Int) = item[position]

    override fun getItemId(position: Int): Long {return 0}

    override fun getCount() = item.size

    private fun createBody(total_count: Int, disabled: Int): String {
        val disabledString: String = context.getString(R.string.disabled)
        val totalCountString: String = context.getString(R.string.total_count)
        return "$totalCountString: $total_count $disabledString: $disabled"
    }

}
