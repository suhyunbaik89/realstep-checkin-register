package com.zeroweb.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.zeroweb.MyApplication
import com.zeroweb.api.ApiService
import com.zeroweb.api.CategorySerializer

import com.zeroweb.checkin.checkin_register.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_search_category.*
import com.zeroweb.model.Categories
import java.util.*


class SearchCategory : DialogFragment() {
    private var listener: OnFragmentInteractionListener? = null
    val apiService = ApiService.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, 0)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search_category, container, false)
    }

    private fun getLanguageType (): String {
        val systemLocale: Locale = MyApplication.instance.resources.configuration.locale
        return if (systemLocale.language == "ja") {
            "JP"
        } else {
            "KR"
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fun context(): Context ?= activity?.applicationContext

        search_category_bar.setIconifiedByDefault(false)
        search_category_bar.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean { return false }

            override fun onQueryTextChange(keyword: String): Boolean {
                val category = CategorySerializer()
                category.keyword = keyword
                category.lang = getLanguageType()

                apiService.categoryList(category)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                        {response ->
                            category_list.adapter = CategoryAdapter(context()!!, response.data)

                    }, { error ->
                        error.printStackTrace()
                    })
                return false
            }
        })

        category_list.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val selectedItem: Categories = parent.getItemAtPosition(position) as Categories
            val i = Intent()
                .putExtra("category_name", selectedItem.name)
                .putExtra("category_id", selectedItem.categoryId);
            targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, i)
            dialog.dismiss()
        }
    }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        fun newInstance(): SearchCategory = SearchCategory()
    }
}

class CategoryAdapter(val context: Context, val item: ArrayList<Categories>) : BaseAdapter(){
    override fun getView(position: Int, converterView: View?, parent: ViewGroup?): View {
        val view: View = LayoutInflater.from(context).inflate(R.layout.category_item, null)

//        val sub_category = view.findViewById<TextView>(R.id.sub_category)
//        val mid_category = view.findViewById<TextView>(R.id.mid_category)

        val name: TextView = view.findViewById(R.id.name)
        val category_id: TextView = view.findViewById(R.id.category_id)
        val category: Categories = item[position]

//        category_id.text = categories.data.id
        name.text = category.name
        category_id.text = category.categoryId.toString()

//        sub_category.text = categories.sub_category
//        mid_category.text = categories.mid_category

        return view
    }

    override fun getItem(position: Int): Categories = item[position]

    override fun getItemId(position: Int): Long {return item.indexOf(getItem(position)).toLong()}

    override fun getCount(): Int = item.size
}
