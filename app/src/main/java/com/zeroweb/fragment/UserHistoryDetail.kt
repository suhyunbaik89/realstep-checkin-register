package com.zeroweb.fragment

import android.content.Context
import android.graphics.Color
import com.zeroweb.utils.Utils.translateWeekday
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.BaseAdapter
import android.widget.TextView
import com.zeroweb.MyApplication
import com.zeroweb.api.ApiService
import com.zeroweb.HistoryDetail
import com.zeroweb.HistorySpot
import com.zeroweb.checkin.checkin_register.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_user_history_detail.*

class UserHistoryDetail : DialogFragment(), AbsListView.OnScrollListener {
    private val history: ArrayList<HistorySpot> by lazy { ArrayList<HistorySpot>().apply { addAll((arguments?.getSerializable("history") as List<*>).filterIsInstance<HistorySpot>()) } }
    private val adapter by lazy { HistoryDetailAdapter(MyApplication.instance, history) }
    var isEnd: Boolean = true
    var currentPage: Int = 1
    var username: String = ""
    var selectedDate: String = ""

    override fun onScroll(view: AbsListView?, firstVisibleItem: Int, visibleItemCount: Int, totalItemCount: Int) {}

    override fun onScrollStateChanged(view: AbsListView?, scrollState: Int) {
         if(scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && isEnd) {
             currentPage += 1
             val apiService = ApiService.create()
             apiService.getSpotHistoryDetail(username = username, page = currentPage, selected_date = selectedDate)
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribeOn(Schedulers.io())
                 .subscribe({ historyDetail: HistoryDetail ->
                     history.addAll(historyDetail.history)
                     adapter.notifyDataSetChanged()
                 }, { error: Throwable ->
                     error.printStackTrace()
                 })
         }
    }

    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, 0)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_user_history_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        history_detail_list.adapter = adapter
        history_detail_list.setOnScrollListener(this)
        val header: TextView = view.findViewById(R.id.history_detail_header)
        val translatedWeekday: String = translateWeekday(arguments?.get("weekday") as String)
        header.text = "%s (%s)".format(arguments?.get("date") as String, translatedWeekday)
        history_count.text = "Total %s".format(arguments?.get("count") as String)
        username = arguments?.get("username") as String
        selectedDate = arguments?.get("selectedDate") as String
    }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        fun newInstance(): UserHistoryDetail = UserHistoryDetail()
    }

    class HistoryDetailAdapter(val context: Context, val item: List<HistorySpot>) : BaseAdapter(){

        override fun getView(position: Int, converterView: View?, parent: ViewGroup?): View {
            val view: View = LayoutInflater.from(context).inflate(R.layout.user_history_detail_item, null)

            val name = view.findViewById<TextView>(R.id.name)
            val category = view.findViewById<TextView>(R.id.category)
            val floor = view.findViewById<TextView>(R.id.floor)
            val road_address = view.findViewById<TextView>(R.id.road_address)
            val disabled = view.findViewById<TextView>(R.id.disabled)

            val spot: HistorySpot = item[position]

            name.text = spot.name
            category.text = spot.category
            floor.text = "%sF".format(spot.floor)
            road_address.text = spot.road_address

            if (spot.disabled) {
                disabled.text = MyApplication.instance.getString(R.string.disabled)
                name.setTextColor(Color.GRAY)
                category.setTextColor(Color.GRAY)
                floor.setTextColor(Color.GRAY)
                road_address.setTextColor(Color.GRAY)
                disabled.setTextColor(Color.GRAY)
            } else { disabled.visibility = View.GONE }
            return view
    }

    override fun getItem(position: Int) = item[position]

    override fun getItemId(position: Int): Long {return item.indexOf(getItem(position)).toLong()}

    override fun getCount(): Int = item.size

    }

}

