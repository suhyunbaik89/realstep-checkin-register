package com.zeroweb.fragment

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.zeroweb.api.ApiService
import com.zeroweb.api.PasswordSerializer
import com.zeroweb.checkin.checkin_register.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_change_password.*

class ChangePassword : Fragment() {
//    private var username: String? = ""
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        change_password_button.setOnClickListener { attemptChangePassword() }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_change_password, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val username = arguments?.getString("username")
        change_password_button.setOnClickListener {
            if (username != null) {
                attemptChangePassword(username)
            }
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

//    override fun onAttach(context: Context) {
//        super.onAttach(context)
//        if (context is OnFragmentInteractionListener) {
//            listener = context
//        } else {
//            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener") as Throwable
//        }
//    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    private fun attemptChangePassword(username:String) {
        val currentStr = current_password.text.toString()
        val newStr = new_password.text.toString()
        val newConfirmStr = new_password_confirm.text.toString()

        if(newStr != newConfirmStr) {
            Toast.makeText(this.context, "비밀번호 재확인", Toast.LENGTH_LONG).show()
        } else {
            val password_serializer = PasswordSerializer()
            password_serializer.username = username
            password_serializer.password = currentStr
            password_serializer.newPasswd = newStr

            val apiService = ApiService.create()
            apiService.changePassword(password_serializer)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ user ->
                    Toast.makeText(this.context, "비밀번호 변경 완료", Toast.LENGTH_LONG).show()
                }, { error ->
                    error.printStackTrace()
//                    Toast.makeText(applicationContext, "비밀번호 변경 실패", Toast.LENGTH_LONG).show()
                })
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment change_password.
         */
        // TODO: Rename and change types and number of parameters
//        @JvmStatic
//        fun newInstance(param1: String, param2: String) =
//            ChangePassword().apply {
//                arguments = Bundle().apply {
//                    putString(ARG_PARAM1, param1)
//                    putString(ARG_PARAM2, param2)
//                }
//            }

        fun newInstance(): ChangePassword = ChangePassword()
    }
}
