package com.zeroweb

import com.zeroweb.model.*
import java.io.Serializable


data class User (
    var data: UserInfo? = null
)

data class Category(
    var data: ArrayList<Categories>
)

data class Device (
    var data: DeviceInfo? = null
)

data class SpotList (
    var data: ArrayList<Spot>
)

data class NewSpotResponse (
    var data: Spot? = null
)

class HistoryDetail (
    var count: Int = 0,
    var current_page : Int = 0,
    var next_page : Int = 0,
    var prev_page : Int = 0,
    var weekday : String = "",
    var date : String = "",
    var history: ArrayList<HistorySpot>,
    var pages: ArrayList<Int>? = null
)

class HistorySpot(val name:String, val road_address:String, val registered_date:String, val category:String, val floor:String, val disabled:Boolean):Serializable {
}

class VisitResponse (
    var data: Spot? = null
)
